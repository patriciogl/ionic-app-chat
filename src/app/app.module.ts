import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';

// Nota: (1) Importa módulos de firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule, AngularFireStorage } from '@angular/fire/storage';

// Nota (2) Credenciales y configuración inicial de firebase
export const firebaseConfig = {
  apiKey: "AIzaSyAk2bthVQtOl_oE1C1FP8mfKESSqz0WVGQ",
  authDomain: "chatdbv2.firebaseapp.com",
  databaseURL: "https://chatdbv2.firebaseio.com",
  projectId: "chatdbv2",
  storageBucket: "chatdbv2.appspot.com",
  messagingSenderId: "23162852049"
};

// Importa páginas (custom elements)
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { UserNamePage} from '../pages/user-name/user-name';
import { CreateAccountPage } from '../pages/create-account/create-account';
import { MessagesProvider } from '../providers/messages/messages';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    UserNamePage,
    CreateAccountPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    // Nota (3) Importa módulos
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    UserNamePage,
    CreateAccountPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // Nota (4) Importa provider firebase database
    AngularFireDatabase,
    AngularFireStorage,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    MessagesProvider,
    Camera,
  ]
})
export class AppModule { }
