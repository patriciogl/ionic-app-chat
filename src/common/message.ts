export interface Message {
    author?: string,
    message?: string,
    date?: string,
    uid?: string,
    id?: string,
    image?: string,
}