import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Message } from '../../common/message';

@Injectable()
export class MessagesProvider {
  public imageRef: any;
  public imageurl: any;
  public donwloadUrl: any;
  constructor(private _db: AngularFireDatabase) {

  }

  public fetchAll() {
    return this._db.list<Message>('messages').valueChanges();
  }

  public add(item) {
    this._db.list('messages').set(item.id, item);
  }
  public createId() {
    return this._db.createPushId();
  }
  public deleteMessage(key) {
    this._db.list("messages").remove(key);
  }

}
