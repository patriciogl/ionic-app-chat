import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the UserNamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-name',
  templateUrl: 'user-name.html',
})
export class UserNamePage {
  myForm: FormGroup;
  usuario: string;
  uid: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder) {
    this.usuario = this.navParams.get("nombre");
    this.uid = this.navParams.get("uid");
    this.myForm = this.formBuilder.group({
      usuario: ['', Validators.required]
    });
  }

   goToHomePage() {
    this.navCtrl.setRoot(HomePage,{usuario:this.usuario, uid:this.uid});
  }

}
