import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Content, NavParams, List } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Message } from '../../common/message';
import { MessagesProvider } from '../../providers/messages/messages';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {DomSanitizer} from '@angular/platform-browser';
 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;
  @ViewChild(List,{read: ElementRef}) chatList: ElementRef;
  public mutationObserver: MutationObserver;
  // Arreglo "observable" con listado de mensajes
  public messages: Observable<Message[]>;
  // Nuevo mensaje
  private newMessage: string;

  private usuario: string;
  private uid: string;
  private idMessage: string;
  private item: Message;
  private image: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private _msgProvider: MessagesProvider, private camera: Camera, public domSanitizer: DomSanitizer) {
    this.messages = this._msgProvider.fetchAll();
    this.usuario = this.navParams.get("usuario");
    this.uid = this.navParams.get("uid");
    this.image = "";
    this.newMessage = "";
  }

  public send(hasImage) {
    this.createMessage(hasImage);
    this._msgProvider.add(this.item);
    this.content.scrollToBottom(0);

  }
  public createMessage(hasImage) {
    let formattedDate = new Date();
    let minute = formattedDate.getMinutes().toString();
    if (minute.length == 1) {
      minute = "0" + minute;
    }
    let dateString = formattedDate.getHours().toString() + ":" + minute;
    this.idMessage = this._msgProvider.createId();
    this.item = {
      author: this.usuario,
      message: this.newMessage,
      date: dateString,
      uid: this.uid,
      id: this.idMessage,
      image: this.image,
    };
    this.newMessage = '';
    this.image = "";

  }

  public deleteMessage(key) {
    this._msgProvider.deleteMessage(key);
  }

  public ionViewDidLoad() {

    this.mutationObserver = new MutationObserver((observer)=>{
      this.content.scrollToBottom();
    });
    this.mutationObserver.observe(this.chatList.nativeElement,{
      childList: true
    });
  }

  public takePic() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then((imageData) => {
     this.image = 'data:image/jpeg;base64, ' + imageData;
     this.send(1);
    }, (err) => {
      console.log(err);
    });

  }
  public takeGalleryPic(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    };
    this.camera.getPicture(options).then((imageData) => {
     this.image = 'data:image/jpeg;base64, ' + imageData;
     this.send(1);
    }, (err) => {
      console.log(err);
    });
   
  }


}
